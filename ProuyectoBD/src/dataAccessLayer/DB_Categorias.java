package dataAccessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class DB_Categorias 
{
     public static void DBInsert(categorias C) throws Exception {
        conexion conec = new conexion();
        Connection con = conec.connect();
            String Select = "insert into category "+"(category_id,name,last_update) values ("+ "'" + C.getId()+ "', '"
				+ C.getNombre() +"', '" + C.getActualizacion()+ "')";
		PreparedStatement st = con.prepareStatement(Select);
		st.executeUpdate();
		con.close();
	}   
    public static categorias DBSelect(int id) throws Exception {
        conexion conec = new conexion();
        Connection con = conec.connect();
	categorias ac  = new categorias();
	String Select = "select * from category where category_id='"+id+"'";
	PreparedStatement st = con.prepareStatement(Select);
	ResultSet rs = st.executeQuery();
	if (!rs.wasNull()) {
		rs.next();
		ac.setId(rs.getInt(1));
		ac.setNombre(rs.getString(2));
                ac.setActualizacion(rs.getString(3));
		}
	con.close();
        return ac;
    }
    public static void DBUpdate(categorias C) throws Exception{
        conexion conec = new conexion();
        Connection con = conec.connect();
            String update = "UPDATE category SET name = '"+C.getNombre()+"',"
                    + "last_update = '"+C.getActualizacion()+"' where category_id = "+C.getId();
		PreparedStatement st = con.prepareStatement(update);
		st.executeUpdate();
		con.close();
    }
    public static void DBDelete(int id) throws Exception{
            int sig = Integer.MAX_VALUE;
            conexion conec = new conexion();
            Connection con = conec.connect();
            String Select = "DELETE FROM category WHERE category_id= '"+id+"'";
		PreparedStatement st = con.prepareStatement(Select);
                st.executeUpdate();
		con.close();
    }
        public static int DBUltimo() throws Exception{
            int sig = Integer.MAX_VALUE;
            conexion conec = new conexion();
            Connection con = conec.connect();
            String Select = "SELECT max(category_id) FROM category";
		PreparedStatement st = con.prepareStatement(Select);
		ResultSet rs = st.executeQuery();
		if (rs != null && rs.next())
			sig = rs.getInt(1) + 1;
		con.close();
		return sig;
    }

}
