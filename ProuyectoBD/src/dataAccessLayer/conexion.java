
package dataAccessLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class conexion 
{
    private final String url = "jdbc:postgresql://localhost/DVDRental";
    private final String user = "postgres";
    private final String password = "12345";
     
    
    public Connection connect() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(this.url, this.user, this.password);
            System.out.println("Conectado al servidor de PostgreSQL exitosamente.");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
 
        return conn;
    }
}
