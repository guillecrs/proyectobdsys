
package dataAccessLayer;

public class actor 
{
    private int id;
    private String nombre;
    private String apellidos;
    private String actualizacion;

    public actor(int id, String nombre, String apellidos, String actualizacion) {
        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.actualizacion = actualizacion;
    }

    public actor(String nombre, String apellidos) {
        this.nombre = nombre;
        this.apellidos = apellidos;
    }

    public actor(String Nombre, String Apellidos, String Fecha) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public actor() {
         //To change body of generated methods, choose Tools | Templates.
    }
    
    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getActualizacion() {
        return actualizacion;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setActualizacion(String actualizacion) {
        this.actualizacion = actualizacion;
    }
    
}
