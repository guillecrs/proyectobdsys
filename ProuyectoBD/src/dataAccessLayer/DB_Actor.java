
package dataAccessLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import dataAccessLayer.conexion;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DB_Actor
{
    
    public static void DBInsert(actor C) throws Exception {
        conexion conec = new conexion();
        Connection con = conec.connect();
            String Select = "insert into actor "+"(actor_id,first_name,last_name,last_update) values ("+ "'" + C.getId()+ "', '"
				+ C.getNombre() + "', '" + C.getApellidos() + "', '" + C.getActualizacion()+ "')";
		PreparedStatement st = con.prepareStatement(Select);
		st.executeUpdate();
		con.close();
	}   
    public static actor DBSelect(int id) throws Exception {
        conexion conec = new conexion();
        Connection con = conec.connect();
	actor ac  = new actor();
	String Select = "SELECT * FROM actor WHERE actor_id='" +id+ "'";
	PreparedStatement st = con.prepareStatement(Select);
	ResultSet rs = st.executeQuery();
	if (!rs.wasNull()) {
		rs.next();
		ac.setId(Integer.parseInt(rs.getString(1)));
		ac.setNombre(rs.getString(2));
		ac.setApellidos(rs.getString(3));
                ac.setActualizacion(rs.getString(4));
		}
	con.close();
        return ac;
    }
    public static void DBUpdate(actor C) throws Exception{
        conexion conec = new conexion();
        Connection con = conec.connect();
            String update = "UPDATE actor SET first_name = '"+C.getNombre()+"',"
                    + "last_name = '"+C.getApellidos()+"', "+ 
                    "last_update = '"+C.getActualizacion()+"' where actor_id = "+C.getId();
		PreparedStatement st = con.prepareStatement(update);
		st.executeUpdate();
		con.close();
    }
    public static void DBDelete(int id) throws Exception{
            int sig = Integer.MAX_VALUE;
            conexion conec = new conexion();
            Connection con = conec.connect();
            String Select = "DELETE FROM actor WHERE actor_id= '"+id+"'";
		PreparedStatement st = con.prepareStatement(Select);
                st.executeUpdate();
		con.close();
    }
        public static int DBUltimo() throws Exception{
            int sig = Integer.MAX_VALUE;
            conexion conec = new conexion();
            Connection con = conec.connect();
            String Select = "SELECT max(actor_id) FROM actor";
		PreparedStatement st = con.prepareStatement(Select);
		ResultSet rs = st.executeQuery();
		if (rs != null && rs.next())
			sig = rs.getInt(1) + 1;
		con.close();
		return sig;
    }
         public static ArrayList<Object[]> LlenaTabla(String apellido)
        {
            ArrayList<Object[]> datos = new ArrayList<Object[]>();
        try {
            conexion conec = new conexion();
            Connection con = conec.connect();

            String Select = "SELECT * FROM actor WHERE last_name= '"+apellido+"'";
            PreparedStatement st = con.prepareStatement(Select);
            ResultSet rs = st.executeQuery();
            try 
            {
                while(rs.next())
                {
                    Object[] filas = new Object[4];
                    for(int i=0;i<4;i++)
                    {
                        filas[i]=rs.getObject(i+1);
                    }
                    datos.add(filas);
                }
            } catch (Exception e) 
            {
            }
        } catch (SQLException ex) {
            Logger.getLogger(DB_Actor.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        return datos;
        }
    public static ArrayList<Object[]> Existencia(){
           ArrayList<Object[]> datos = new ArrayList<Object[]>();
        try {
            conexion conec = new conexion();
            Connection con = conec.connect();

            String Select = "select (Select city from  city\n" +
            " inner join  address on address.city_id=city.city_id\n" +
            " inner join store on store.address_id=address.address_id where store_id=inventory.store_id) as \"Nombre Tienda\","+
            "(select title from film where (select inventory.film_id ) =film_id) as \"Nombre Pelicula\",count(film_id) as existencia \n" +
            "from inventory group by film_id,store_id  order by film_id,store_id";
            PreparedStatement st = con.prepareStatement(Select);
            ResultSet rs = st.executeQuery();
            try 
            {
                while(rs.next())
                {
                    Object[] filas = new Object[3];
                    for(int i=0;i<3;i++)
                    {
                        filas[i]=rs.getObject(i+1);
                    }
                    datos.add(filas);
                }
            } catch (Exception e) 
            {
            }
        } catch (SQLException ex) {
            Logger.getLogger(DB_Actor.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        return datos;
    }
}
