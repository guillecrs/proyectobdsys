
package dataAccessLayer;

public class categorias 
{
    private int id;
    private String nombre;
    private String actualizacion;

    public categorias(int id, String nombre, String actualizacion) {
        this.id = id;
        this.nombre = nombre;
        this.actualizacion = actualizacion;
    }

    public categorias(String nombre) {
        this.nombre = nombre;
    }

    public categorias() 
    {
    }
    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getActualizacion() {
        return actualizacion;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setActualizacion(String actualizacion) {
        this.actualizacion = actualizacion;
    }
    
}
