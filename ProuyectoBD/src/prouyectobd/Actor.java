package prouyectobd;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.String;
import dataAccessLayer.DB_Actor;
import dataAccessLayer.actor;
import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import dataAccessLayer.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JTextField;

public class Actor extends javax.swing.JFrame {


    public Actor() {
        initComponents();
        setLocationRelativeTo(null);
        String num;
        try {
            num = Integer.toString(DB_Actor.DBUltimo());
            jTextField1.setText(num);
            jTextField4.setText(getFecha());
        } catch (Exception ex) {
            Logger.getLogger(Actor.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "NO SE PUDO CARGAR EL ID DE ACTOR", "Aviso del Sistema", JOptionPane.INFORMATION_MESSAGE);
        }
    }
    public String getFecha()
    {
        Date date = new Date ();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss ");
        String formatDate = sdf.format(date);
        return formatDate;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton4 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();

        jButton4.setText("jButton4");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("GESTION DE ACTORES");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setText("ID DE ACTOR");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(19, 30, -1, -1));

        jLabel2.setText("NOMBRE");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(19, 77, -1, -1));

        jLabel3.setText("APELLIDOS");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(19, 117, -1, -1));

        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });
        getContentPane().add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(102, 27, 98, -1));
        jTextField1.getAccessibleContext().setAccessibleName("txtId");

        getContentPane().add(jTextField2, new org.netbeans.lib.awtextra.AbsoluteConstraints(79, 74, 167, -1));
        jTextField2.getAccessibleContext().setAccessibleName("txtnombre");

        getContentPane().add(jTextField3, new org.netbeans.lib.awtextra.AbsoluteConstraints(91, 114, 219, -1));
        jTextField3.getAccessibleContext().setAccessibleName("txtapellidos");

        jLabel4.setText("ULTIMA ACTUALIZACION:");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(19, 154, -1, -1));

        jTextField4.setEditable(false);
        jTextField4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jTextField4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jTextField4MouseExited(evt);
            }
        });
        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });
        getContentPane().add(jTextField4, new org.netbeans.lib.awtextra.AbsoluteConstraints(163, 151, 199, -1));
        jTextField4.getAccessibleContext().setAccessibleName("txtactualixacion");

        jButton1.setText("GUARDAR");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 202, -1, -1));

        jButton2.setText("ELIMINAR");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(109, 202, -1, -1));

        jButton3.setText("EDITAR");
        getContentPane().add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(208, 202, -1, -1));

        jButton5.setText("ACTUALIZAR");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(295, 202, -1, -1));

        jButton6.setText("SALIR");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(408, 202, -1, -1));

        jButton7.setText("LIMPIAR");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton7, new org.netbeans.lib.awtextra.AbsoluteConstraints(396, 100, -1, -1));

        jButton8.setText("BUSCAR");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton8, new org.netbeans.lib.awtextra.AbsoluteConstraints(396, 148, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        int Id=Integer.parseInt(jTextField1.getText());
        String Nombre=jTextField2.getText();
        String Apellidos=jTextField3.getText();
        
        //String Fecha=jTextField4.getText();
        actor reg = new actor(Id,Nombre,Apellidos,getFecha());
        try {
            DB_Actor.DBInsert(reg);
            JOptionPane.showMessageDialog(null, "Se Inserto El Nuevo Actor", "Aviso del Sistema", JOptionPane.INFORMATION_MESSAGE);
            String num=null;
            num = Integer.toString(DB_Actor.DBUltimo());
            jTextField1.setText(num);
            jTextField2.setText("");
            jTextField3.setText("");
            jTextField4.setText("");
        } catch (Exception ex) {
            Logger.getLogger(Actor.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex, "Aviso del Sistema", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        if (JOptionPane.showConfirmDialog(null, "Estas Seguro Que Deseas Salir Al Menu Principal", "Aviso Del Sistema", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
            AppPcpal app = new AppPcpal();
            app.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        try {
            actor ac;
            ac=DB_Actor.DBSelect(Integer.parseInt(jTextField1.getText()));
            jTextField2.setText(ac.getNombre());
            jTextField3.setText(ac.getApellidos());
            jTextField4.setText(ac.getActualizacion());
        } catch (Exception ex) {
            Logger.getLogger(Actor.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex, "Aviso del Sistema", JOptionPane.WARNING_MESSAGE);            
        }

    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        jTextField1.setText(null);
        jTextField2.setText(null);
        jTextField3.setText(null);
        jTextField4.setText(null); 
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed
        
    }//GEN-LAST:event_jTextField4ActionPerformed

    private void jTextField4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextField4MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField4MouseEntered

    private void jTextField4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextField4MouseExited
        // TODO add your handling code here: 
    }//GEN-LAST:event_jTextField4MouseExited

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        int Id=Integer.parseInt(jTextField1.getText());
        String Nombre=jTextField2.getText();
        String Apellidos=jTextField3.getText();
        //String Fecha=jTextField4.getText();
        actor reg = new actor(Id,Nombre,Apellidos,getFecha());
          try {
            DB_Actor.DBUpdate(reg);
            JOptionPane.showMessageDialog(null, "Se actualizaron los datos del Actor", "Aviso del Sistema", JOptionPane.INFORMATION_MESSAGE);
            jTextField1.setText("");
            jTextField2.setText("");
            jTextField3.setText("");
            jTextField4.setText("");
        } catch (Exception ex) {
            Logger.getLogger(Actor.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex, "Aviso del Sistema", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if(jTextField1.getText().equals(""))
        {
             JOptionPane.showMessageDialog(null,"INDIQUE UN ID DE ACTOR PARA PODER ELIMINARLO", "Aviso del Sistema", JOptionPane.WARNING_MESSAGE);
        }
        else
        {
            try {
            int id = Integer.parseInt(jTextField1.getText());
            DB_Actor.DBDelete(id);
            JOptionPane.showMessageDialog(null,"SE ELIMINO EL ACTOR INDICADO", "Aviso del Sistema", JOptionPane.WARNING_MESSAGE);
            jTextField1.setText("");
            jTextField2.setText("");
            jTextField3.setText("");
            jTextField4.setText("");
        } catch (Exception ex) {
            Logger.getLogger(Actor.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex, "Aviso del Sistema", JOptionPane.WARNING_MESSAGE);
        }
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new Actor().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    // End of variables declaration//GEN-END:variables
}
