************************************
*************INTEGRANTES************
** CUEVAS MADRIGAL ALEJANDRO      **
** CRUZ SALAZAR GUILLERMO ARMANDO **
************************************
************************************

En equipo de 2 personas, Escribir las consultas que resuelvan las siguientes
dudas:
1. Elaborar un sistema que tenga la siguientes opciones:
a. Altas, Bajas y cambios de categorías
b. Altas, Bajas y cambios de Actores
2. Consulta de actores por apellido
3. Consulta de existencias por tienda
a. Responder con un reporte con la siguiente estructura
Nombre Tienda Nombre Película Existencia
Culiacán XXXXXXX 10 
MORELIA YYYYYYY 5
4. Alta de rentas (1 sola película por renta)
5. Devoluciones de rentas
1. Usar Store Procedures
2. Usar Transacciones